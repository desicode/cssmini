### minicss

A micro css framework for building modern webapps

### Install

``npm install @desicode/cssmini``

### Usage

```js
import '@desicode/cssmini';
```
